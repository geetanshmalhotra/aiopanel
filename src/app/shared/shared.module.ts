import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AutocompleteLibModule
  ],
})
export class SharedModule { }
