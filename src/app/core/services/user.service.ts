import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { User } from '../models/auth.models';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class UserProfileService {
    constructor(private httpClient: HttpClient) { }

    getAll() {
        return this.httpClient.get<User[]>(`/api/login`);
    }

    postValue(url, data) {
		let token = localStorage.getItem('token');
		const headers = new HttpHeaders({
			'Content-Type': 'application/json'
		});
		return this.httpClient.post(`${environment.url}/${url}`, data, { headers: headers })
			.pipe(map(res => {
				return res;
			}),
				catchError((err) => {
					return throwError(err);
				}))
    }
    
    getValue(url) {
		let token = localStorage.getItem('token');
		const headers = new HttpHeaders({
			'Content-Type': 'application/json'
		});
		return this.httpClient.get(`${environment.url}/${url}`, { headers: headers })
			.pipe(map(res => {
				return res;
			}),
				catchError((err) => {
					return throwError(err);
				}))
	}
}