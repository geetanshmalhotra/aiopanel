import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { OrderHistoryComponent } from './order-history/order-history.component';
import { NewOrderComponent } from './new-order/new-order.component';
import { ServiceListComponent } from './service-list/service-list.component';
import { SupportComponent } from './support/support.component';


const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'new-order', component: NewOrderComponent},
  { path: 'order-history', component: OrderHistoryComponent},
  { path: 'add-funds', component: OrderHistoryComponent},
  { path: 'support', component: SupportComponent},
  { path: 'services', component: ServiceListComponent},

  { path: 'ui', loadChildren: () => import('./ui/ui.module').then(m => m.UiModule) },
  { path: 'apps', loadChildren: () => import('./apps/apps.module').then(m => m.AppsModule) },
  { path: 'other', loadChildren: () => import('./other/other.module').then(m => m.OtherModule) },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
