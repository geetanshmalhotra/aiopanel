import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { NgApexchartsModule } from 'ng-apexcharts';
import { FlatpickrModule } from 'angularx-flatpickr';

import { UIModule } from '../shared/ui/ui.module';
import { PagesRoutingModule } from './pages-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';

import { WidgetModule } from '../shared/widgets/widget.module';
import { UiModule } from './ui/ui.module';
import { AppsModule } from './apps/apps.module';
import { OtherModule } from './other/other.module';
import { OrderHistoryComponent } from './order-history/order-history.component';
import { NewOrderComponent } from './new-order/new-order.component';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import { SupportComponent } from './support/support.component';
import { ServiceListComponent } from './service-list/service-list.component';

@NgModule({
  declarations: [DashboardComponent, OrderHistoryComponent, NewOrderComponent, SupportComponent, ServiceListComponent],
  imports: [
    CommonModule,
    FormsModule,
    NgbDropdownModule,
    NgApexchartsModule,
    FlatpickrModule.forRoot(),
    UIModule,
    WidgetModule,
    PagesRoutingModule,
    UiModule,
    AppsModule,
    OtherModule,
    AutocompleteLibModule
  ]
})
export class PagesModule { }
