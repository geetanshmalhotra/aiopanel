import { Component, OnInit } from '@angular/core';
import { UserProfileService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-new-order',
  templateUrl: './new-order.component.html',
  styleUrls: ['./new-order.component.scss']
})
export class NewOrderComponent implements OnInit {

  keyword = 'name';
  data = [
     {
       id: 1,
       name: 'Usa'
     },
     {
       id: 2,
       name: 'England'
     }
  ];
  serviceList: any;

  constructor(
    private userProfileService : UserProfileService
  ) { }

  ngOnInit() {
    this.getServices()
  }
  getServices() {
    this.userProfileService.getValue('/api/getServices').subscribe(async(res) =>{
      debugger;
      this.serviceList = res;
    },
    err=>{
      console.log(err)
    })
  }

}
